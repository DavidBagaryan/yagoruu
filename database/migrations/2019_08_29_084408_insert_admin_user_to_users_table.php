<?php

use App\Role;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Str;

class InsertAdminUserToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('users')->insert([
            'name' => 'admin.us',
            'email' => 'admin.us@admin.us',
            'created_at' => now(),
            'email_verified_at' => now(),
            'password' => Hash::make('i_am_your_father'),
            'remember_token' => Str::random(10),
            'role_id' => Role::ADMIN
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('users')->where([
            'email' => 'admin.us@admin.us'
        ])->delete();
    }
}
