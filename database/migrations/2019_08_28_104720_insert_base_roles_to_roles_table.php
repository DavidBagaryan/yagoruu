<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class InsertBaseRolesToRolesTable extends Migration
{
    /**
     * basic roles to fill out
     * @var array
     */
    private $roles = [
        'intern'  => 0,
        'junior'  => 1,
        'regular' => 2,
        'middle'  => 3,
        'senior'  => 4,
        'leader'  => 5,
        'guru'    => 998,
        'admin'   => 999,
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $idCounter = 1;

        foreach ($this->roles as $role => $value) {
            DB::table('roles')->insert([
                'id'            => $idCounter,
                'name'          => $role,
                'numeric_value' => $value
            ]);

            $idCounter ++;
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->roles as $role => $value) {
            DB::table('roles')->where([
                'name'          => $role,
                'numeric_value' => $value
            ])->delete();
        }
    }
}
