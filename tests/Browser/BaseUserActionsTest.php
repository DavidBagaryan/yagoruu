<?php

namespace Tests\Browser;

use App\Question;
use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Throwable;

class BaseUserActionsTest extends DuskTestCase
{
    use DatabaseMigrations;

    const CREATE_QUESTION_BUTTON = 'this is a button to add a question';
    const JUMP_TO_QUESTIONS = 'I am ready to hear you, Bro';
    const NO_QUESTION_YET = 'there is not question yet, Bro';
    const POPULAR = 'popular:';

    /**
     * the User with role less than `guru`
     * @var User
     */
    private $regular;

    /**
     * the User with role equals or more than `guru`
     * @var User
     */
    private $master;

    /**
     * @inheritDoc
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->regular = factory(User::class)->create([
            'email'   => 'from-intern-to-leader@boddy.you',
            'role_id' => rand(1, 6)
        ]);

        $this->master = factory(User::class)->create([
            'email'   => 'guru-or-more@mind.all',
            'role_id' => rand(7, 8)
        ]);
    }

    /**
     * testing possibility to create a question by regular person and by master
     *
     * @test
     * @return void
     * @throws Throwable
     */
    public function a_user_can_create_a_question(): void
    {
        $this->browse(function (Browser $first, Browser $second) {

            $first->loginAs($this->regular)
                  ->visit('/')
                  ->assertPathIs('/questions')
                  ->waitForLink($this->regular->name)
                  ->assertDontSeeLink(self::CREATE_QUESTION_BUTTON)
                  ->logout();

            $second->loginAs($this->master)
                   ->visit('/')
                   ->assertPathIs('/questions')
                   ->waitForLink($this->master->name)
                   ->waitForLink(self::CREATE_QUESTION_BUTTON)
                   ->assertSeeLink(self::CREATE_QUESTION_BUTTON)
                   ->logout();
        });
    }

    /**
     * @test
     * @return void
     * @throws Throwable
     */
    public function user_can_see_the_questions(): void
    {
        factory(Question::class, 19)->create();

        $this->browse(function (Browser $first, Browser $second, Browser $third) {

            $first->loginAs($this->regular)
                  ->visit('/questions')
                  ->waitForText(self::POPULAR)
                  ->assertSee(self::POPULAR)
                  ->logout();

            $second->loginAs($this->master)
                   ->visit('/questions')
                   ->waitForText(self::POPULAR)
                   ->assertSee(self::POPULAR)
                   ->assertSeeLink(self::CREATE_QUESTION_BUTTON)
                   ->logout();

            Question::query()->delete();

            $third->loginAs($this->regular)
                  ->visit('/questions')
                  ->waitForText(self::NO_QUESTION_YET)
                  ->assertSee(self::NO_QUESTION_YET)
                  ->logout();
        });
    }

    /**
     * @test
     * @group links
     * @return void
     * @throws Throwable
     */
    public function master_can_add_a_question(): void
    {
        $this->browse(function (Browser $browser){
            $browser->loginAs($this->master)
                    ->visit('/questions')
                    ->assertSeeLink(self::CREATE_QUESTION_BUTTON)
                    ->clickLink(self::CREATE_QUESTION_BUTTON)
                    ->assertPathIs('/questions/create');
        });
    }
}
