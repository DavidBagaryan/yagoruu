<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Throwable;

class AuthTest extends DuskTestCase
{
    use DatabaseMigrations;

    const USER_MAIL = 'susanochka@baby.com';

    /**
     * @test
     * @return void
     * @throws Throwable
     */
    public function redirect_unregistered_user_to_login_page(): void
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/home')->assertRouteIs('login');
        });
    }

    /**
     * @test
     * @return void
     * @throws Throwable
     */
    public function user_registration(): void
    {
        /** @var @var User $user */
        $user = factory(User::class)->make();

        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/')
                    ->assertSeeLink('Login')
                    ->assertSeeLink('Register')
                    ->clickLink('Register')
                    ->assertPathIs('/register')
                    ->type('name', $user->name)
                    ->type('email', $user->email)
                    ->type('password', $user->password)
                    ->type('password_confirmation', $user->password)
                    ->press('Register')
                    ->assertPathIs('/home')
                    ->assertSeeLink($user->name)
                    ->assertSee('Dashboard');
        });

        $this->assertDatabaseHas('users', ['email' => $user->email]);
    }

    /**
     * test user possibility login.
     *
     * @test
     * @return void
     * @throws Throwable
     */
    public function a_user_can_login(): void
    {
        /*** @var User $user */
        $user = factory(User::class)->create([
            'email' => self::USER_MAIL,
        ]);

        $this->browse(function (Browser $browser) use ($user) {
            $userName = $user->getAttributeValue('name');

            $browser->loginAs($user)
                    ->visit('/')
                    ->waitForLink($userName)
                    ->assertSeeLink($userName)
                    ->logout();
        });
    }

    /**
     * test user possibility to login (step by step without bypassing).
     *
     * @test
     * @return void
     * @throws Throwable
     */
    public function user_login_step_by_step(): void
    {
        /*** @var User $user */
        $user = factory(User::class)->create([
            'email' => self::USER_MAIL,
        ]);

        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/')
                    ->waitForLink('Login')
                    ->assertSeeLink('Login')
                    ->assertSeeLink('Register')
                    ->clickLink('Login')
                    ->assertPathIs('/login')
                    ->type('email', $user->getAttributeValue('email'))
                    ->type('password', 'password')
                    ->press('Login')
                    ->waitForLocation('/home')
                    ->assertPathIs('/home')
                    ->assertSee('Dashboard')
                    ->assertSeeLink($user->getAttribute('name'))
                    ->logout();
        });
    }

    /**
     * @test
     * @return void
     * @throws Throwable
     */
    public function a_user_can_logout(): void
    {
        /*** @var User $user */
        $user = factory(User::class)->create([
            'email' => self::USER_MAIL,
        ]);

        $this->browse(function (Browser $browser) use ($user) {
            $userName = $user->getAttributeValue('name');

            $browser->loginAs($user)
                    ->visit('/')
                    ->assertSeeLink($userName)
                    ->clickLink($userName)
                    ->clickLink('Logout')
                    ->assertSeeLink('Login')
                    ->assertSeeLink('Register');
        });
    }
}
