<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Throwable;

class LogoTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @test
     * @return void
     * @throws Throwable
     */
    public function a_user_can_see_logo(): void
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')->assertSee(config('app.name'));
        });
    }
}
