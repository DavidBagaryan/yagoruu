@extends('layouts.questions-answers')

@section('content')

    <h3>{{ $answer->content }}</h3>
    <div class="btn-group">

        @if (\auth()->check() && \auth()->user()->can('update', $answer))
            <div>
                <a class="btn btn-primary" href="{{ route('question.answer.edit', [
                'question' => $question,
                'answer' => $answer,
            ]) }}">edit</a>
            </div>
        @endif

        @if (\auth()->check() && \auth()->user()->can('delete', $answer))
            <div>
                <form method="post" action="{{ route('question.answer.destroy', [
                'question' => $question,
                'answer' => $answer,
            ]) }}">

                    @method('DELETE')
                    @csrf

                    <button class="btn btn-danger" type="submit">delete</button>
                </form>
            </div>
        @endif

    </div>
    <hr/>
    <div>author: {{ $answer->author->email }}</div>

@endsection
