@if (count($answers))
    <hr/>
    <h5>last answers:</h5>
    <div>
        <table>
            @foreach($answers as $answer)
                <tr>
                    <td>
                        <a href="{{ route('question.answer.show', [
                            'question' => $question,
                            'answer' => $answer,
                        ]) }}">{{ mb_substr($answer->content, 0, 20) . '...' }}</a>
                    </td>
                    <td>
                        {{ ' ~ at: ' . ($answer->apdeted_at ?: $answer->created_at) . ' ~ ' }}
                    </td>
                    <td>
                        {{  'from:  ' . $answer->author->email}}
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
@endif
