@extends('layouts.questions-answers')

@section('content')
    <div>from: {{ $question->author->email }}</div>
    <h3>{{ $question->content }}</h3>

    <div class="btn-group">
        @if (\auth()->check() && \auth()->user()->can('update', $question))
            <a class="btn btn-primary" href="{{ route('questions.edit', ['question' => $question]) }}">
                edit question
            </a>
        @endif

        @if (\auth()->check() && \auth()->user()->can('delete', $question))
            <form method="post" action="{{ route('questions.destroy', ['question' => $question]) }}">

                @method('DELETE')
                @csrf

                <button class="btn btn-danger" type="submit">you can delete this question</button>
            </form>
        @endif
    </div>

    <hr/>

    <form method="post" action="{{ route('question.answer.store', ['question' => $question]) }}">

        @method('POST')
        @csrf

        <div>
            <label for="content">your answer bellow: </label>
        </div>
        <div>
            <textarea id="content" name="content" placeholder="write here anything...">{{ old('content') }}</textarea>
        </div>

        <hr/>
        @include('errors')

        <div>
            <button type="submit">push to add</button>
        </div>

    </form>

    @include('answers.answers-list', ['answers' => $question->answers])

@endsection

