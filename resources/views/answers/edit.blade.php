@extends('layouts.questions-answers')

@section('content')

    <div>
        question was: <h3>{{ $question->content }}</h3>
    </div>

    <hr/>

    <form method="post" action="{{ route('question.answer.update', [
        'question' => $question,
        'answer' => $answer,
    ]) }}">

        @method('PATCH')
        @csrf

        <div>
            <label for="content">меняй ОТВЕТ бля:</label>
        </div>
        <div>
            <textarea id="content"
                      name="content"
                      placeholder="че там, давай пиши сюда">{{ old('content') ?: $answer->content }}</textarea>
        </div>

        <hr/>
        @include('errors')

        <div>
            <button class="btn btn-primary" type="submit">change</button>
        </div>

    </form>

@endsection
