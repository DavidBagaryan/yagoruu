@extends('layouts.questions-answers')

@section('content')

    <div>
        <h2> {{ $question->content }} </h2>
    </div>

    @if (\auth()->check() && \auth()->user()->can('update', $question))
        <div>
            <a href="{{ route('questions.edit', ['question' => $question]) }}">редактировать вопрос</a>
        </div>
    @endif

    <div>
        <a href="{{ route('question.answer.create', ['question' => $question]) }}">дать ответ!</a>
    </div>

    @include('answers.answers-list', ['answers' => $question->answers])

@endsection
