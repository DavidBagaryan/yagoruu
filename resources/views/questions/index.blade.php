@extends('layouts.questions-answers')

@section('content')

    @if (\auth()->check() && \auth()->user()->isAdmin())
        <a href="{{ route('questions.create') }}">this is a button to add a question</a>
        <hr/>
    @endif

    @if (!count($questions))
        <h3>there is not question yet, Bro</h3>
    @else
        <h5>popular:</h5>
        <hr/>
        <table>
            @foreach($questions as $question)

                <tr>
                    <td>
                        <a href="{{ route('question.answer.create', ['question' => $question]) }}">
                            {{ $question->content }}
                        </a>
                    </td>

                    <td>
                        <div>
                            @if (\auth()->check() && \auth()->user()->can('update', $question))
                                <a href="{{ route('questions.edit', ['question' => $question]) }}">
                                    === поправим вопрос ===
                                </a>
                            @else
                                <a href="{{ route('questions.show', ['question' => $question]) }}">
                                    === посмотреть ответы ===
                                </a>
                            @endif

                            @if ($question->answers->count())
                                ({{ $question->answers->count() }})
                            @endif
                        </div>
                    </td>
                </tr>
            @endforeach
        </table>
    @endif

@endsection
