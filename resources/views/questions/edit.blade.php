@extends('layouts.questions-answers')

@section('content')

    <h1>{{ $question->content }}</h1>

    <form id="question-update" method="post" action="{{ route('questions.update', ['question' => $question]) }}">

        @method('PATCH')
        @csrf

        <div>
            <label for="content">меняй вопрос бля</label>
        </div>
        <div>
            <textarea id="content"
                      name="content"
                      placeholder="че там, давай пиши сюда">{{ old('content') ?: $question->content }}</textarea>
        </div>
    </form>

    @include('errors')

    <form id="question-delete" method="post" action="{{ route('questions.destroy', ['question' => $question]) }}">

        @method('DELETE')
        @csrf

    </form>

    <div class="btn-group">
        <button form="question-update" class="btn btn-primary" type="submit">change</button>
        <button form="question-delete" class="btn btn-danger" type="submit">delete this</button>
    </div>


@endsection
