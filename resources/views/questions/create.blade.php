@extends('layouts.questions-answers')

@section('content')
    <h1>Добавь вопрос по-братски</h1>

    <form method="post" action="{{ route('questions.store') }}">

        @method('POST')
        @csrf

        <div>
            <label for="content">сюда пиши вопрос: </label>
            <input id="content" name="content" placeholder="че ты?" value="{{ old('content') }}"/>
        </div>

        <div>
            <button type="submit">давай жми, вопрос добавим</button>
        </div>

        @include('errors')

    </form>
@endsection
