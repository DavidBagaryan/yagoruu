<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class Question
 * @package App
 * @property HasMany   answers
 * @property BelongsTo author
 */
class Question extends Model
{
    public static $validators = [
        'content' => 'required|min:10|max:255',
    ];

    protected $table = 'questions-for-users';

    protected $guarded = [];

    /**
     * @param array $validAnswer
     */
    public function addAnswer(array $validAnswer)
    {
        if (!array_key_exists('content', $validAnswer)) {
            throw new BadRequestHttpException('no answer data in request!');
        }

        $validAnswer = array_merge($validAnswer, [
            'question_id' => $this->getAttribute('id'),
            'author_id'   => auth()->id()
        ]);

        $this->answers()->create($validAnswer);
    }

    /**
     * @return HasMany
     */
    public function answers()
    {
        return $this->hasMany(Answer::class)
            ->orderBy('updated_at', 'desc')
            ->orderBy('created_at', 'desc');
    }

    /**
     * @return BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
