<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Question;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class QuestionAnswerController extends Controller
{
    /**
     * QuestionController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth')->except('show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Question $question
     * @return Response
     */
    public function create(Question $question)
    {
        return view('answers.create', ['question' => $question]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request  $request
     * @param Question $question
     * @return Response
     * @throws ValidationException
     */
    public function store(Request $request, Question $question)
    {
        $validAnswer = $this->validate($request, Answer::$validators);
        $question->addAnswer($validAnswer);

        return redirect()->route('questions.show', ['question' => $question]);
    }

    /**
     * Display the specified resource.
     *
     * @param Question $question
     * @param Answer   $answer
     * @return Response
     */
    public function show(Question $question, Answer $answer)
    {
        return view('answers.show', [
            'question' => $question,
            'answer'   => $answer,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Question $question
     * @param Answer   $answer
     * @return Response
     * @throws AuthorizationException
     */
    public function edit(Question $question, Answer $answer)
    {
        $this->authorize('update', $answer);

        return view('answers.edit', [
            'question' => $question,
            'answer'   => $answer,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request  $request
     * @param Question $question
     * @param Answer   $answer
     * @return Response
     * @throws AuthorizationException
     * @throws ValidationException
     */
    public function update(Request $request, Question $question, Answer $answer)
    {
        $this->authorize('update', $answer);
        $validated = $this->validate($request, Answer::$validators);
        $answer->update($validated);

        return redirect()->route('questions.show', ['question' => $question]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Question $question
     * @param Answer   $answer
     * @return Response
     * @throws AuthorizationException
     * @throws Exception
     */
    public function destroy(Question $question, Answer $answer)
    {
        $this->authorize('delete', $answer);
        $answer->delete();

        return redirect()->route('questions.index');
    }
}
