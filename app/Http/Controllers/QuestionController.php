<?php

namespace App\Http\Controllers;

use App\Question;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class QuestionController extends Controller
{
    /**
     * QuestionController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('questions.index', [
            'questions' => Question::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', Question::class);
        return view('questions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     * @throws ValidationException
     * @throws AuthorizationException
     */
    public function store(Request $request)
    {
        $this->authorize('create', Question::class);

        $validated = $this->validate($request, Question::$validators);
        $validated['user_id'] = auth()->id();
        Question::create($validated);

        /* TODO
         * or this explicit way
         * $project = Project::create($validated);
        event(new ProjectCreated($project));
        */

        return redirect()->route('questions.index');
    }

    /**
     * Display the specified resource.
     *
     * @param Question $question
     * @return Response
     */
    public function show(Question $question)
    {
        return view('questions.show', ['question' => $question]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Question $question
     * @return Response
     * @throws AuthorizationException
     */
    public function edit(Question $question)
    {
        $this->authorize('update', $question);
        return view('questions.edit', ['question' => $question]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request  $request
     * @param Question $question
     * @return Response
     * @throws AuthorizationException
     * @throws ValidationException
     */
    public function update(Request $request, Question $question)
    {
        $this->authorize('update', $question);
        $validated = $this->validate($request, Question::$validators);
        $question->update($validated);

        return redirect()->route('questions.show', ['question' => $question]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Question $question
     * @return Response
     * @throws AuthorizationException
     * @throws Exception
     */
    public function destroy(Question $question)
    {
        $this->authorize('delete', $question);

        $question->delete();
        return redirect()->route('questions.index');
    }
}
